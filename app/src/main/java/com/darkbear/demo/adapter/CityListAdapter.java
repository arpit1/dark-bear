package com.darkbear.demo.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import com.darkbear.demo.R;
import com.darkbear.demo.activity.MainActivity;
import com.darkbear.demo.pojo.CitiesPojo;

import java.util.ArrayList;

/**
 * Created by Arpit on 7/30/2017.
 */

public class CityListAdapter extends RecyclerView.Adapter<CityListAdapter.ViewHolder> {

    private MainActivity ctx;
    private ArrayList<CitiesPojo> cityList;
    private int lastPosition = -1;

    public CityListAdapter(MainActivity ctx, ArrayList<CitiesPojo> cityList) {
        this.ctx = ctx;
        this.cityList = cityList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.city_list_row_layout, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.cityName.setText(cityList.get(position).getCity());
        holder.stateName.setText(cityList.get(position).getState());
        holder.distance.setText("60mi");

        setAnimation(holder.itemView, position);
    }

    @Override
    public int getItemCount() {
        return cityList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private TextView cityName;
        private TextView stateName;
        private TextView distance;

        public ViewHolder(View itemView) {
            super(itemView);
            cityName = (TextView) itemView.findViewById(R.id.city_name);
            stateName = (TextView) itemView.findViewById(R.id.state_name);
            distance = (TextView) itemView.findViewById(R.id.distance);
        }
    }

    /**
     * Here is the key method to apply the animation
     */
    private void setAnimation(View viewToAnimate, int position) {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition) {
            Animation animation = AnimationUtils.loadAnimation(ctx,
                    (position > lastPosition) ? R.anim.up_from_bottom
                            : R.anim.down_from_top);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }

    @Override
    public void onViewDetachedFromWindow(ViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        holder.itemView.clearAnimation();
    }
}
