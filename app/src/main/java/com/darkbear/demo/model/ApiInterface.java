package com.darkbear.demo.model;

import com.darkbear.demo.pojo.CitiesPojo;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.GET;


/**
 * Created by Arpit on 7/30/2017.
 */

public interface ApiInterface {

    @GET("alternative-fuel-locations.json")
    Call<ArrayList<CitiesPojo>> getCities();
}
