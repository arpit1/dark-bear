package com.darkbear.demo.pojo;

/**
 * Created by Arpit on 7/30/2017.
 */

public class CitiesPojo {

    private String street_address;
    private String station_phone;
    private String status_code;
    private String city;
    private String latitude;
    private String fuel_type_code;
    private String cards_accepted;
    private long updated_at;
    private String lpg_primary;
    private String date_last_confirmed;
    private String id;
    private String state;
    private String intersection_directions;
    private String geocode_status;
    private String longitude;
    private String station_name;
    private String zip;
    private String owner_type_code;
    private String groups_with_access_code;
    private String access_days_time;
    public LocationPojo location;

    public String getStreet_address() {
        return street_address;
    }

    public void setStreet_address(String street_address) {
        this.street_address = street_address;
    }

    public String getStation_phone() {
        return station_phone;
    }

    public void setStation_phone(String station_phone) {
        this.station_phone = station_phone;
    }

    public String getStatus_code() {
        return status_code;
    }

    public void setStatus_code(String status_code) {
        this.status_code = status_code;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getFuel_type_code() {
        return fuel_type_code;
    }

    public void setFuel_type_code(String fuel_type_code) {
        this.fuel_type_code = fuel_type_code;
    }

    public String getCards_accepted() {
        return cards_accepted;
    }

    public void setCards_accepted(String cards_accepted) {
        this.cards_accepted = cards_accepted;
    }

    public long getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(long updated_at) {
        this.updated_at = updated_at;
    }

    public String getLpg_primary() {
        return lpg_primary;
    }

    public void setLpg_primary(String lpg_primary) {
        this.lpg_primary = lpg_primary;
    }

    public String getDate_last_confirmed() {
        return date_last_confirmed;
    }

    public void setDate_last_confirmed(String date_last_confirmed) {
        this.date_last_confirmed = date_last_confirmed;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getIntersection_directions() {
        return intersection_directions;
    }

    public void setIntersection_directions(String intersection_directions) {
        this.intersection_directions = intersection_directions;
    }

    public String getGeocode_status() {
        return geocode_status;
    }

    public void setGeocode_status(String geocode_status) {
        this.geocode_status = geocode_status;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getStation_name() {
        return station_name;
    }

    public void setStation_name(String station_name) {
        this.station_name = station_name;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public String getOwner_type_code() {
        return owner_type_code;
    }

    public void setOwner_type_code(String owner_type_code) {
        this.owner_type_code = owner_type_code;
    }

    public String getGroups_with_access_code() {
        return groups_with_access_code;
    }

    public void setGroups_with_access_code(String groups_with_access_code) {
        this.groups_with_access_code = groups_with_access_code;
    }

    public String getAccess_days_time() {
        return access_days_time;
    }

    public void setAccess_days_time(String access_days_time) {
        this.access_days_time = access_days_time;
    }

    public LocationPojo getLocation() {
        return location;
    }

    public void setLocation(LocationPojo location) {
        this.location = location;
    }
}
