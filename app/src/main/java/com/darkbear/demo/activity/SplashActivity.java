package com.darkbear.demo.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

import com.darkbear.demo.R;

public class SplashActivity extends AppCompatActivity {

    private SplashActivity ctx = this;
    private Handler splashTimeHandler;
    private Runnable finalizer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        goAhead();
    }

    private void goAhead() {
        splashTimeHandler = new Handler();
        finalizer = new Runnable() {
            public void run() {
                Intent mainIntent = new Intent(ctx, MainActivity.class);
                startActivity(mainIntent);
                finish();
            }
        };
        splashTimeHandler.postDelayed(finalizer, 1000);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (splashTimeHandler != null && finalizer != null)
            splashTimeHandler.removeCallbacks(finalizer);
    }
}
