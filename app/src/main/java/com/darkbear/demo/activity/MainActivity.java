package com.darkbear.demo.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.darkbear.demo.R;
import com.darkbear.demo.adapter.CityListAdapter;
import com.darkbear.demo.model.ApiClient;
import com.darkbear.demo.model.ApiInterface;
import com.darkbear.demo.pojo.CitiesPojo;
import com.darkbear.demo.util.ConnectionDetector;
import com.darkbear.demo.util.DBConstants;
import com.darkbear.demo.util.DBDialog;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    private MainActivity ctx = this;
    private RecyclerView.LayoutManager mLayoutManager;
    private RecyclerView.Adapter cityListAdapter;
    private RecyclerView cities_list;
    private EditText search;
    private ConnectionDetector cd;
    private DBDialog dialog;
    private ArrayList<CitiesPojo> main_arr;
    private ArrayList<CitiesPojo> filtered_arr;
    private int PLACE_PICKER_REQUEST = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        cities_list = (RecyclerView) findViewById(R.id.cities_list);
        search = (EditText) findViewById(R.id.search_location);
        ImageView cross = (ImageView) findViewById(R.id.cross);
        TextView select = (TextView) findViewById(R.id.select);

        cities_list.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
        cities_list.setLayoutManager(mLayoutManager);
        cities_list.setNestedScrollingEnabled(false);

        cd = new ConnectionDetector(getApplicationContext());
        dialog = new DBDialog(ctx);

        search.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                filtered_arr = new ArrayList<>();
                String text = search.getText().toString();
                if(main_arr != null && main_arr.size() > 0) {
                    for (int i = 0; i < main_arr.size(); i++) {
                        if (text != null && main_arr.get(i).getCity().toLowerCase().contains(text.toLowerCase()))
                            filtered_arr.add(main_arr.get(i));
                    }
                    cityListAdapter = new CityListAdapter(ctx, filtered_arr);
                    cities_list.setAdapter(cityListAdapter);
                    cityListAdapter.notifyDataSetChanged();
                }
            }
        });

        cross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                search.setText("");
                if(main_arr != null) {
                    cityListAdapter = new CityListAdapter(ctx, main_arr);
                    cities_list.setAdapter(cityListAdapter);
                    cityListAdapter.notifyDataSetChanged();
                }
            }
        });

        select.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    PlacePicker.IntentBuilder intentBuilder =
                            new PlacePicker.IntentBuilder();
//                    intentBuilder.setLatLngBounds(BOUNDS_MOUNTAIN_VIEW);
                    Intent intent = intentBuilder.build(ctx);
                    startActivityForResult(intent, PLACE_PICKER_REQUEST);

                } catch (GooglePlayServicesRepairableException
                        | GooglePlayServicesNotAvailableException e) {
                    e.printStackTrace();
                }
            }
        });

        getCityList();

    }

    private void getCityList() {
        if (cd.isConnectingToInternet()) {
            final ProgressDialog d = DBDialog.showLoading(ctx);
            d.setCanceledOnTouchOutside(false);
            ApiInterface apiService =
                    ApiClient.getClient().create(ApiInterface.class);

            Call<ArrayList<CitiesPojo>> call = apiService.getCities();
            System.out.println("retrofit URL " + call.request());
            call.enqueue(new Callback<ArrayList<CitiesPojo>>() {
                @Override
                public void onResponse(Call<ArrayList<CitiesPojo>> call, Response<ArrayList<CitiesPojo>> response) {
                    if (response.body() != null) {
                        main_arr = response.body();
                        cityListAdapter = new CityListAdapter(ctx, response.body());
                        cities_list.setAdapter(cityListAdapter);
                        d.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<ArrayList<CitiesPojo>> call, Throwable t) {
                    // Log error here since request failed
                    System.out.println("retrofit hh failure " + t.getMessage());
                    d.dismiss();
                }
            });
        } else {
            dialog.displayCommonDialog(DBConstants.NO_INTERNET_CONNECTED);
        }
    }

    @Override
    protected void onActivityResult(int requestCode,
                                    int resultCode, Intent data) {

        if (requestCode == PLACE_PICKER_REQUEST
                && resultCode == Activity.RESULT_OK) {

            final Place place = PlacePicker.getPlace(this, data);
            final CharSequence name = place.getName();
            final CharSequence address = place.getAddress();
            String attributions = (String) place.getAttributions();
            if (attributions == null) {
                attributions = "";
            }

            search.setText(name);

        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }
}
