package com.darkbear.demo.util;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.darkbear.demo.R;

/**
 * Created by Arpit on 7/30/2017.
 */

public class DBDialog {
    private Activity ctx;
    private Dialog lDialog;
    private boolean closeActivity = true;

    public static ProgressDialog showLoading(Activity activity) {
        ProgressDialog mProgressDialog = new ProgressDialog(activity);
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage("Loading...");
        mProgressDialog.show();
        return mProgressDialog;
    }


    public DBDialog(Activity ctx) {
        this.ctx = ctx;
    }

    public void displayCommonDialog(String msg) {
        Button OkButtonLogout;
        final Dialog DialogLogOut = new Dialog(ctx, android.R.style.Theme_Translucent_NoTitleBar);
        DialogLogOut.setContentView(R.layout.custom_alert_dialog);
        TextView loogout_msg = (TextView) DialogLogOut.findViewById(R.id.text_exit);
        loogout_msg.setText(msg);
        OkButtonLogout = (Button) DialogLogOut.findViewById(R.id.btn_yes_exit);
        Button CancelButtonLogout = (Button) DialogLogOut.findViewById(R.id.btn_no_exit);
        CancelButtonLogout.setVisibility(View.GONE);

        OkButtonLogout.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                DialogLogOut.dismiss();
            }
        });
        CancelButtonLogout.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                DialogLogOut.dismiss();
            }
        });
        DialogLogOut.show();
    }

}
